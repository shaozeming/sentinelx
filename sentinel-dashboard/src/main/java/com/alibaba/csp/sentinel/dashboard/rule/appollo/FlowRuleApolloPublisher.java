/*
 * Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alibaba.csp.sentinel.dashboard.rule.appollo;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.rule.AbstractDynamicRulePublisher;
import com.alibaba.csp.sentinel.dashboard.rule.convert.Converter;
import com.alibaba.csp.sentinel.dashboard.rule.util.ConfigIdUtil;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.ctrip.framework.apollo.openapi.client.ApolloOpenApiClient;
import com.ctrip.framework.apollo.openapi.dto.NamespaceReleaseDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenItemDTO;

import java.util.List;

/**
 * @author: shaozeming
 * @date: 2022/8/8 15:24
 * @description:
 **/
public class FlowRuleApolloPublisher extends AbstractDynamicRulePublisher {

    private ApolloOpenApiClient apolloOpenApiClient;

    private ApolloConfig apolloConfig;

    public FlowRuleApolloPublisher(ApolloOpenApiClient apolloOpenApiClient,ApolloConfig apolloConfig){
        this.apolloOpenApiClient =apolloOpenApiClient;
        this.apolloConfig =apolloConfig;
    }

    @Override
    protected void publishPrivate(String app, List<RuleEntity> rules, Class<?> clz) throws Exception {
        AssertUtil.notEmpty(app, "app name cannot be empty");
        if (rules == null) {
            return;
        }
        publish(ConfigIdUtil.getKey(clz), Converter.encode(rules));
    }

    @Override
    protected void publishDefault(String app, List<RuleEntity> rules, Class<?> clz) throws Exception {
        AssertUtil.notEmpty(app, "app name cannot be empty");
        if (rules == null) {
            return;
        }
        String defaultKey = ConfigIdUtil.getDefaultKey(clz);
        if(defaultKey ==null ||defaultKey.length() ==0){
            return;
        }
        publish(defaultKey, Converter.encode(rules));
    }

    private void publish(String flowDataId,String value){

        OpenItemDTO openItemDTO = new OpenItemDTO();
        openItemDTO.setKey(flowDataId);
        openItemDTO.setValue(value);
        openItemDTO.setComment("发布规则");
        openItemDTO.setDataChangeCreatedBy("sentinel-dashboard");
        apolloOpenApiClient.createOrUpdateItem(apolloConfig.getAppId(), apolloConfig.getEnv(), apolloConfig.getClusterName(), apolloConfig.getNamespaceName(), openItemDTO);

        // Release configuration
        NamespaceReleaseDTO namespaceReleaseDTO = new NamespaceReleaseDTO();
        namespaceReleaseDTO.setEmergencyPublish(true);
        namespaceReleaseDTO.setReleaseComment("发布规则");
        namespaceReleaseDTO.setReleasedBy("sentinel-dashboard");
        namespaceReleaseDTO.setReleaseTitle("发布规则");
        apolloOpenApiClient.publishNamespace(apolloConfig.getAppId(), apolloConfig.getEnv(), apolloConfig.getClusterName(), apolloConfig.getNamespaceName(), namespaceReleaseDTO);
    }


}
