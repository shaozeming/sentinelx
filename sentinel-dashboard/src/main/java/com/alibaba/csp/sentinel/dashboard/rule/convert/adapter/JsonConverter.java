package com.alibaba.csp.sentinel.dashboard.rule.convert.adapter;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author: shaozeming
 * @date: 2022/8/30 10:50
 * @description:
 **/
public class JsonConverter<T> extends SentinelConverter {

    public JsonConverter(ObjectMapper objectMapper, Class<T> ruleClass) {
        super(objectMapper, ruleClass);
    }

}