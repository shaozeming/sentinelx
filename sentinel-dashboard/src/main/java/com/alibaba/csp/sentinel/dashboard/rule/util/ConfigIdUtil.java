package com.alibaba.csp.sentinel.dashboard.rule.util;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.gateway.ApiDefinitionEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.gateway.GatewayFlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.*;

import static com.alibaba.csp.sentinel.dashboard.rule.util.CommonConstant.*;

/**
 * @author: shaozeming
 * @date: 2022/8/23 19:45
 * @description:
 **/
public class ConfigIdUtil {


    public static String getDefaultKey(Class<?> clz){
        if (clz.equals(AuthorityRuleEntity.class) ) {
            return   DEFAULT_AUTHORITY_DATA_ID_POSTFIX;
        } else if (clz.equals(DegradeRuleEntity.class)) {
            return   DEFAULT_DEGRADE_DATA_ID_POSTFIX;
        } else if (clz.equals(FlowRuleEntity.class) ) {
            return  DEFAULT_FLOW_DATA_ID_POSTFIX ;
        }
        return null ;
    }


    public static String getKey(Class<?> clz) {
        if (clz.equals(AuthorityRuleEntity.class) ) {
            return   AUTHORITY_DATA_ID_POSTFIX;
        } else if (clz.equals(DegradeRuleEntity.class)) {
            return   DEGRADE_DATA_ID_POSTFIX;
        } else if (clz.equals(FlowRuleEntity.class) ) {
            return  FLOW_DATA_ID_POSTFIX ;
        } else if(clz.equals(ParamFlowRuleEntity.class) ) {
            return   PARAM_FLOW_DATA_ID_POSTFIX;
        } else if (clz.equals(SystemRuleEntity.class) ) {
            return  SYSTEM_DATA_ID_POSTFIX ;
        }else if (clz.equals(GroupRuleEntity.class) ) {
            return  GROUP_DATA_ID_POSTFIX ;
        }else if (clz.equals(GatewayFlowRuleEntity.class) ) {
            return  GATEWAY_ID_POSTFIX ;
        }else if (clz.equals(ApiDefinitionEntity.class) ) {
            return  GATEWAY_API_ID_POSTFIX ;
        }
        return null ;
    }
}
