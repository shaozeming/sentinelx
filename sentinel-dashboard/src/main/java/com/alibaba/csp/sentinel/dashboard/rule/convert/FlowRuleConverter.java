package com.alibaba.csp.sentinel.dashboard.rule.convert;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.DegradeRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.FlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.discovery.AppInfo;
import com.alibaba.csp.sentinel.dashboard.discovery.MachineInfo;
import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.csp.sentinel.slots.block.AbstractRule;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: shaozeming
 * @date: 2022/8/8 15:24
 * @description:
 **/
public class FlowRuleConverter extends AbstractRuleConverter {


    @Override
    Class<FlowRule> getRuleClass() {
        return FlowRule.class;
    }

    @Override
    RuleEntity fromRule(AbstractRule abstractRule, AppInfo appInfo, List<MachineInfo> machineInfoList) {
          return FlowRuleEntity.fromFlowRule(appInfo.getApp(),machineInfoList.get(0).getIp(),machineInfoList.get(0).getPort(),(FlowRule)abstractRule);
    }
}
