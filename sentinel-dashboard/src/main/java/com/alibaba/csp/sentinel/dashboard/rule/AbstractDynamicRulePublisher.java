/*
 * Copyright 1999-2018 Alibaba Group Holding Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alibaba.csp.sentinel.dashboard.rule;


import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Eric Zhao
 * @since 1.4.0
 */
public abstract class AbstractDynamicRulePublisher implements DynamicRulePublisher<List<RuleEntity>>{

    private static final String DEFAULT_NAME="DEFAULT";

    /**
     * Publish rules to remote rule configuration center for given application name.
     *
     * @param app app name
     * @param rules list of rules to push
     * @throws Exception if some error occurs
     */
    public   void publish(String app, List<RuleEntity> rules) throws Exception{

    }


    /**
     * Publish rules to remote rule configuration center for given application name.
     *
     * @param app app name
     * @param rules list of rules to push
     * @throws Exception if some error occurs
     */
    public void publish(String app, List<RuleEntity> rules ,Class<?> clz) throws Exception{

        List<RuleEntity> defaultList = rules.stream().filter(ruleEntity ->DEFAULT_NAME.equals(ruleEntity.getResource())).collect(Collectors.toList());
        List<RuleEntity> privateList = rules.stream().filter(ruleEntity -> !DEFAULT_NAME.equals(ruleEntity.getResource())).collect(Collectors.toList());
        publishDefault(app,defaultList,clz);
        publishPrivate(app,privateList,clz);
    }


   protected abstract void publishPrivate(String app, List<RuleEntity> rules ,Class<?> clz) throws Exception;


    protected  abstract void publishDefault(String app, List<RuleEntity> rules ,Class<?> clz) throws Exception;

}
