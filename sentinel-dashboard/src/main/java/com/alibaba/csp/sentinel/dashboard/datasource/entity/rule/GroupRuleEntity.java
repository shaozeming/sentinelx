package com.alibaba.csp.sentinel.dashboard.datasource.entity.rule;

import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.util.AssertUtil;
import com.szm.sentinelx.slots.defaults.group.GroupRule;


/**
 * @author: shaozeming
 * @date: 2022/8/18 14:14
 * @description:
 **/
public class GroupRuleEntity extends AbstractRuleEntity<GroupRule> {


    private String groupName;


    private String resource;


    public GroupRuleEntity() {
    }

    public GroupRuleEntity(GroupRule groupRule) {
        AssertUtil.notNull(groupRule, "GroupRule rule should not be null");
        this.rule = groupRule;
    }

    public static GroupRuleEntity fromGroupRule(String app, String ip, Integer port, GroupRule rule) {
        GroupRuleEntity entity = new GroupRuleEntity(rule);
        entity.setApp(app);
        entity.setIp(ip);
        entity.setPort(port);
        entity.setGroupName(rule.getGroupName());
        entity.setResource(rule.getResource());
        return entity;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public GroupRule toRule() {
        GroupRule groupRule = new GroupRule();
        groupRule.setGroupName(this.groupName);
        groupRule.setResource(this.resource);
        return groupRule;
    }
}
