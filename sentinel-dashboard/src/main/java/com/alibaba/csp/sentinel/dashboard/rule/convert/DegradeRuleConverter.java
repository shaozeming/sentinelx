package com.alibaba.csp.sentinel.dashboard.rule.convert;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.AuthorityRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.DegradeRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.discovery.AppInfo;
import com.alibaba.csp.sentinel.dashboard.discovery.MachineInfo;
import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.csp.sentinel.slots.block.AbstractRule;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: shaozeming
 * @date: 2022/8/8 15:24
 * @description:
 **/
public class DegradeRuleConverter extends AbstractRuleConverter {


    @Override
    Class<DegradeRule> getRuleClass() {
        return DegradeRule.class;
    }

    @Override
    RuleEntity fromRule(AbstractRule abstractRule, AppInfo appInfo, List<MachineInfo> machineInfoList) {
        return DegradeRuleEntity.fromDegradeRule(appInfo.getApp(),machineInfoList.get(0).getIp(),machineInfoList.get(0).getPort(),(DegradeRule)abstractRule);
    }
}
