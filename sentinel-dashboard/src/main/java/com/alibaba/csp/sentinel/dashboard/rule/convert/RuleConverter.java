package com.alibaba.csp.sentinel.dashboard.rule.convert;


import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.discovery.AppInfo;
import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.fastjson.JSON;


import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: shaozeming
 * @date: 2022/8/8 15:25
 * @description:
 **/
public interface RuleConverter {


   default Converter<List<RuleEntity>, String> ruleEntityEncoder(){
       return v-> JSON.toJSONString(v.stream().map(RuleEntity::toRule).collect(Collectors.toList()));
   }

    default Converter<String, List<RuleEntity>> ruleEntityDecoder(AppInfo appInfo){
       return null;
    }





}
