package com.alibaba.csp.sentinel.dashboard.rule.convert;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.GroupRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.discovery.AppInfo;
import com.alibaba.csp.sentinel.dashboard.discovery.MachineInfo;
import com.alibaba.csp.sentinel.slots.block.AbstractRule;
import com.szm.sentinelx.slots.defaults.group.GroupRule;

import java.util.List;

/**
 * @author: shaozeming
 * @date: 2022/8/8 15:24
 * @description:
 **/
public class GroupRuleConverter extends AbstractRuleConverter {


    @Override
    Class<GroupRule> getRuleClass() {
        return GroupRule.class;
    }

    @Override
    RuleEntity fromRule(AbstractRule abstractRule, AppInfo appInfo, List<MachineInfo> machineInfoList) {
          return GroupRuleEntity.fromGroupRule(appInfo.getApp(),machineInfoList.get(0).getIp(),machineInfoList.get(0).getPort(),(GroupRule) abstractRule);
    }
}
