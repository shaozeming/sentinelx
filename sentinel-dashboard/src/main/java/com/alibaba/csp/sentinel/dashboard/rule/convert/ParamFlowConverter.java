package com.alibaba.csp.sentinel.dashboard.rule.convert;

import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.FlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.ParamFlowRuleEntity;
import com.alibaba.csp.sentinel.dashboard.datasource.entity.rule.RuleEntity;
import com.alibaba.csp.sentinel.dashboard.discovery.AppInfo;
import com.alibaba.csp.sentinel.dashboard.discovery.MachineInfo;
import com.alibaba.csp.sentinel.datasource.Converter;
import com.alibaba.csp.sentinel.slots.block.AbstractRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRule;
import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: shaozeming
 * @date: 2022/8/8 15:24
 * @description:
 **/
public class ParamFlowConverter extends AbstractRuleConverter {


    @Override
    Class<ParamFlowRule> getRuleClass() {
        return ParamFlowRule.class;
    }

    @Override
    RuleEntity fromRule(AbstractRule abstractRule, AppInfo appInfo, List<MachineInfo> machineInfoList) {
        return ParamFlowRuleEntity.fromParamFlowRule(appInfo.getApp(),machineInfoList.get(0).getIp(),machineInfoList.get(0).getPort(),(ParamFlowRule)abstractRule);
    }
}
