package com.alibaba.csp.sentinel.dashboard.rule.util;

/**
 * @author: shaozeming
 * @date: 2022/8/23 19:29
 * @description:
 **/
public class CommonConstant {


    public static final String AUTHORITY_DATA_ID_POSTFIX = "-authority-rules";
    public static final String DEGRADE_DATA_ID_POSTFIX = "-degrade-rules";
    public static final String FLOW_DATA_ID_POSTFIX = "-flow-rules";
    public static final String PARAM_FLOW_DATA_ID_POSTFIX = "-param-rules";
    public static final String SYSTEM_DATA_ID_POSTFIX = "-system-rules";

    public static final String GATEWAY_ID_POSTFIX = "-gateway-rules";

    public static final String GATEWAY_API_ID_POSTFIX = "-gateway-api-rules";

    public static final String GROUP_DATA_ID_POSTFIX = "-group-rules";

    public static final String DEFAULT_AUTHORITY_DATA_ID_POSTFIX = "-default-authority-rules";

    public static final String DEFAULT_DEGRADE_DATA_ID_POSTFIX = "-default-degrade-rules";

    public static final String DEFAULT_FLOW_DATA_ID_POSTFIX = "-default-flow-rules";


    public static final String CLUSTER_MAP_DATA_ID_POSTFIX = "-cluster-map";

    /**
     * cc for `cluster-client`
     */
    public static final String CLIENT_CONFIG_DATA_ID_POSTFIX = "-cc-config";
    /**
     * cs for `cluster-server`
     */
    public static final String SERVER_TRANSPORT_CONFIG_DATA_ID_POSTFIX = "-cs-transport-config";
    public static final String SERVER_FLOW_CONFIG_DATA_ID_POSTFIX = "-cs-flow-config";
    public static final String SERVER_NAMESPACE_SET_DATA_ID_POSTFIX = "-cs-namespace-set";

}
