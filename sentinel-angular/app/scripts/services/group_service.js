var app = angular.module('sentinelDashboardApp');

app.service('GroupService', ['$http', function ($http) {
    this.queryMachineRules = function (app, ip, port) {
        var param = {
            app: app,
            ip: ip,
            port: port
        };
        return $http({
            url: '/group/rules',
            params: param,
            method: 'GET'
        });
    };

    this.newRule = function (rule) {
        var param = {
            resource: rule.resource,
            groupName: rule.groupName,
            app: rule.app,
            ip: rule.ip,
            port: rule.port
        };

        return $http({
            url: '/group/rule',
            data: param,
            method: 'POST'
        });
    };

    this.saveRule = function (rule) {
        var param = {
            id: rule.id,
            resource: rule.resource,
            groupName: rule.groupName,
            app: rule.app,
            ip: rule.ip,
            port: rule.port
        };

        return $http({
            url: '/group/rule/'+rule.id,
            data: param,
            method: 'PUT'
        });
    };

    this.deleteRule = function (rule) {
        var param = {
            id: rule.id,
            app: rule.app
        };

        return $http({
            url: '/group/rule/'+rule.id,
            params: param,
            method: 'DELETE'
        });
    };

    function notNumberAtLeastZero(num) {
        return num === undefined || num === '' || isNaN(num) || num < 0;
    }

    function notNumberGreaterThanZero(num) {
        return num === undefined || num === '' || isNaN(num) || num <= 0;
    }

    this.checkRuleValid = function (rule) {
        if (rule.resource === undefined || rule.resource === '') {
            alert('资源名称不能为空');
            return false;
        }
        if (rule.groupName === undefined || rule.groupName === '') {
            alert('分组名称不能为空');
            return false;
        }
       
        return true;
    };
}]);
