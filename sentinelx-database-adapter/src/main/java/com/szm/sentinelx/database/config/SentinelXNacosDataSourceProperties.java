package com.szm.sentinelx.database.config;

import com.alibaba.cloud.sentinel.datasource.config.AbstractDataSourceProperties;
import com.alibaba.cloud.sentinel.datasource.factorybean.NacosDataSourceFactoryBean;
import com.alibaba.csp.sentinel.datasource.AbstractDataSource;
import com.szm.sentinelx.database.SentinelXRegister;
import org.springframework.util.StringUtils;


/**
 * @author: shaozeming
 * @date: 2022/8/13 13:54
 * @description: nacos数据源配置
 **/
public class SentinelXNacosDataSourceProperties  extends AbstractDataSourceProperties {



    private String serverAddr;

    private String username;

    private String password;

    private String groupId = "DEFAULT_GROUP";

    private String dataId;

    private String endpoint;

    private String namespace;

    private String accessKey;

    private String secretKey;

    public SentinelXNacosDataSourceProperties() {
        super(NacosDataSourceFactoryBean.class.getName());
    }

    @Override
    public void preCheck(String dataSourceName) {
        if (StringUtils.isEmpty(serverAddr)) {
            serverAddr = this.getEnv().getProperty(
                    "spring.cloud.sentinel.datasource.nacos.server-addr",
                    "localhost:8848");
        }
    }

    public String getServerAddr() {
        return serverAddr;
    }

    public void setServerAddr(String serverAddr) {
        this.serverAddr = serverAddr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }


    @Override
    public void postRegister(AbstractDataSource dataSource) {
        SentinelXRegister.postRegister(this.getRuleType(),dataSource);
    }
}
