package com.szm.sentinelx.database;

import com.alibaba.cloud.sentinel.datasource.RuleType;
import com.alibaba.csp.sentinel.datasource.AbstractDataSource;
import com.szm.sentinelx.slots.defaults.authority.DefaultAuthorityRuleManager;
import com.szm.sentinelx.slots.defaults.degrade.DefaultDegradeManager;
import com.szm.sentinelx.slots.defaults.flow.DefaultFlowRuleManager;

/**
 * @author: shaozeming
 * @date: 2022/8/13 14:09
 * @description: 默认规则动态数据源注册
 **/
public class SentinelXRegister {


    public static void postRegister(RuleType ruleType,AbstractDataSource dataSource) {
        switch (ruleType) {
            case FLOW:
                DefaultFlowRuleManager.register2Property(dataSource.getProperty());
                break;
            case DEGRADE:
                DefaultDegradeManager.register2Property(dataSource.getProperty());
                break;
            case AUTHORITY:
                DefaultAuthorityRuleManager.register2Property(dataSource.getProperty());
                break;
            default:
                break;

        }
    }
}
