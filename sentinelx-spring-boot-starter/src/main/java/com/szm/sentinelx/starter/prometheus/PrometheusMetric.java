package com.szm.sentinelx.starter.prometheus;

import com.alibaba.csp.sentinel.metric.extension.MetricExtension;
import com.alibaba.csp.sentinel.slots.block.BlockException;

/**
 * @author: shaozeming
 * @date: 2022/9/14 19:30
 * @description:
 **/
public class PrometheusMetric implements MetricExtension {

    private static class  DEFAULT implements MetricExtension{

        /**
         * Add current pass count of the resource name.
         *
         * @param resource resource name
         * @param n        count to add
         * @param args     additional arguments of the resource, eg. if the resource is a method name,
         *                 the args will be the parameters of the method.
         */
        @Override
        public void addPass(String resource, int n, Object... args) {

        }

        /**
         * Add current block count of the resource name.
         *
         * @param resource       resource name
         * @param n              count to add
         * @param origin         the original invoker.
         * @param blockException block exception related.
         * @param args           additional arguments of the resource, eg. if the resource is a method name,
         *                       the args will be the parameters of the method.
         */
        @Override
        public void addBlock(String resource, int n, String origin, BlockException blockException, Object... args) {

        }

        /**
         * Add current completed count of the resource name.
         *
         * @param resource resource name
         * @param n        count to add
         * @param args     additional arguments of the resource, eg. if the resource is a method name,
         *                 the args will be the parameters of the method.
         */
        @Override
        public void addSuccess(String resource, int n, Object... args) {

        }

        /**
         * Add current exception count of the resource name.
         *
         * @param resource  resource name
         * @param n         count to add
         * @param throwable exception related.
         */
        @Override
        public void addException(String resource, int n, Throwable throwable) {

        }

        /**
         * Add response time of the resource name.
         *
         * @param resource resource name
         * @param rt       response time in millisecond
         * @param args     additional arguments of the resource, eg. if the resource is a method name,
         *                 the args will be the parameters of the method.
         */
        @Override
        public void addRt(String resource, long rt, Object... args) {

        }

        /**
         * Increase current thread count of the resource name.
         *
         * @param resource resource name
         * @param args     additional arguments of the resource, eg. if the resource is a method name,
         *                 the args will be the parameters of the method.
         */
        @Override
        public void increaseThreadNum(String resource, Object... args) {

        }

        /**
         * Decrease current thread count of the resource name.
         *
         * @param resource resource name
         * @param args     additional arguments of the resource, eg. if the resource is a method name,
         *                 the args will be the parameters of the method.
         */
        @Override
        public void decreaseThreadNum(String resource, Object... args) {

        }
    }



    private static  MetricExtension METRIC_EXTENSION= new DEFAULT();


    public static void setMetricExtension(MetricExtension metricExtension){
       METRIC_EXTENSION =metricExtension;
    }

    /**
     * Add current pass count of the resource name.
     *
     * @param resource resource name
     * @param n        count to add
     * @param args     additional arguments of the resource, eg. if the resource is a method name,
     *                 the args will be the parameters of the method.
     */
    @Override
    public void addPass(String resource, int n, Object... args) {
        METRIC_EXTENSION.addPass(resource,n,args);
    }

    /**
     * Add current block count of the resource name.
     *
     * @param resource       resource name
     * @param n              count to add
     * @param origin         the original invoker.
     * @param blockException block exception related.
     * @param args           additional arguments of the resource, eg. if the resource is a method name,
     *                       the args will be the parameters of the method.
     */
    @Override
    public void addBlock(String resource, int n, String origin, BlockException blockException, Object... args) {
        METRIC_EXTENSION.addBlock(resource,n,origin,blockException,args);
    }

    /**
     * Add current completed count of the resource name.
     *
     * @param resource resource name
     * @param n        count to add
     * @param args     additional arguments of the resource, eg. if the resource is a method name,
     *                 the args will be the parameters of the method.
     */
    @Override
    public void addSuccess(String resource, int n, Object... args) {
        METRIC_EXTENSION.addSuccess(resource,n,args);
    }

    /**
     * Add current exception count of the resource name.
     *
     * @param resource  resource name
     * @param n         count to add
     * @param throwable exception related.
     */
    @Override
    public void addException(String resource, int n, Throwable throwable) {
        METRIC_EXTENSION.addException(resource,n,throwable);
    }

    /**
     * Add response time of the resource name.
     *
     * @param resource resource name
     * @param rt       response time in millisecond
     * @param args     additional arguments of the resource, eg. if the resource is a method name,
     *                 the args will be the parameters of the method.
     */
    @Override
    public void addRt(String resource, long rt, Object... args) {
        METRIC_EXTENSION.addRt(resource,rt,args);
    }

    /**
     * Increase current thread count of the resource name.
     *
     * @param resource resource name
     * @param args     additional arguments of the resource, eg. if the resource is a method name,
     *                 the args will be the parameters of the method.
     */
    @Override
    public void increaseThreadNum(String resource, Object... args) {
        METRIC_EXTENSION.increaseThreadNum(resource,args);
    }

    /**
     * Decrease current thread count of the resource name.
     *
     * @param resource resource name
     * @param args     additional arguments of the resource, eg. if the resource is a method name,
     *                 the args will be the parameters of the method.
     */
    @Override
    public void decreaseThreadNum(String resource, Object... args) {
        METRIC_EXTENSION.decreaseThreadNum(resource,args);
    }
}
