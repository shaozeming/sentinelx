package com.szm.sentinelx.starter.endpoint;

import com.alibaba.cloud.sentinel.SentinelProperties;
import com.alibaba.cloud.sentinel.endpoint.SentinelEndpoint;
import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnAvailableEndpoint;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @author: shaozeming
 * @date: 2022/9/14 19:07
 * @description:
 **/
@ConditionalOnClass(Endpoint.class)
@EnableConfigurationProperties({ SentinelProperties.class })
public class SentinelXEndpointAutoConfiguration {

    @Bean
    @ConditionalOnAvailableEndpoint
    public SentinelEndpoint sentinelEndPoint(SentinelProperties sentinelProperties) {
        return new SentinelXEndpoint(sentinelProperties);
    }
}
