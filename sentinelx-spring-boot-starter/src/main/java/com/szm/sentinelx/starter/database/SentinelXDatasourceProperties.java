package com.szm.sentinelx.starter.database;


import com.szm.sentinelx.database.config.SentinelXDataSourcePropertiesConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author: shaozeming
 * @date: 2022/8/13 13:42
 * @description: 数据源配置
 **/
@ConfigurationProperties(prefix = "sentinelx.default")
public class SentinelXDatasourceProperties {


    private Map<String, SentinelXDataSourcePropertiesConfiguration> datasource = new TreeMap<>(
            String.CASE_INSENSITIVE_ORDER);


    public void setDatasource(Map<String, SentinelXDataSourcePropertiesConfiguration> datasource) {
        this.datasource = datasource;
    }

    public Map<String, SentinelXDataSourcePropertiesConfiguration> getDatasource() {
        return datasource;
    }


}
