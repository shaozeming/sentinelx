package com.szm.sentinelx.starter;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author: shaozeming
 * @date: 2022/8/14 11:14
 * @description:
 **/
@Configuration
@EnableConfigurationProperties(SentinelXProperties.class)
public class SentinelXAutoconfiguration {
}
