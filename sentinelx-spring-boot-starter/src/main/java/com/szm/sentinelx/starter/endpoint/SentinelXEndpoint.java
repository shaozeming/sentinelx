package com.szm.sentinelx.starter.endpoint;

import com.alibaba.cloud.sentinel.SentinelProperties;
import com.alibaba.cloud.sentinel.endpoint.SentinelEndpoint;
import com.alibaba.csp.sentinel.config.SentinelConfig;
import com.alibaba.csp.sentinel.log.LogBase;
import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRuleManager;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.alibaba.csp.sentinel.slots.block.flow.param.ParamFlowRuleManager;
import com.alibaba.csp.sentinel.slots.system.SystemRuleManager;
import com.alibaba.csp.sentinel.transport.config.TransportConfig;
import com.alibaba.csp.sentinel.util.AppNameUtil;
import com.szm.sentinelx.slots.defaults.authority.DefaultAuthorityRuleManager;
import com.szm.sentinelx.slots.defaults.degrade.DefaultDegradeManager;
import com.szm.sentinelx.slots.defaults.flow.DefaultFlowRuleManager;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;

import java.util.HashMap;
import java.util.Map;

import static com.alibaba.cloud.sentinel.SentinelConstants.BLOCK_PAGE_URL_CONF_KEY;

/**
 * @author: shaozeming
 * @date: 2022/9/14 18:55
 * @description:
 **/
@Endpoint(id = "sentinel")
public class SentinelXEndpoint extends SentinelEndpoint {
    public SentinelXEndpoint(SentinelProperties sentinelProperties) {
        super(sentinelProperties);
    }


    @ReadOperation
    public Map<String, Object> invoke() {
        final Map<String, Object> result = super.invoke();
        final Map<String, Object> rules = new HashMap<>();
        result.put("defaultRules", rules);
        rules.put("flowRules", DefaultFlowRuleManager.getBackRules());
        rules.put("degradeRules", DefaultDegradeManager.getBackRules());
        rules.put("authorityRule", DefaultAuthorityRuleManager.getBackRules());
        return result;
    }
}
