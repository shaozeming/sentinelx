package com.szm.sentinelx.starter.prometheus;

import io.prometheus.client.CollectorRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.export.prometheus.PrometheusMetricsExportAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;

/**
 * @author: shaozeming
 * @date: 2022/9/14 21:08
 * @description:
 **/
@ConditionalOnBean(PrometheusMetricsExportAutoConfiguration.class)
@AutoConfigureAfter(PrometheusMetricsExportAutoConfiguration.class)
public class PrometheusAutoConfiguration {


    @Bean
    @ConditionalOnBean(CollectorRegistry.class)
    public PrometheusMetricLoader prometheusMetricLoader(CollectorRegistry collectorRegistry){
        return new PrometheusMetricLoader(collectorRegistry);
    }
}
