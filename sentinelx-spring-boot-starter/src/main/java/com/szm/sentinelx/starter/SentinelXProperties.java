package com.szm.sentinelx.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: shaozeming
 * @date: 2022/8/14 11:00
 * @description:
 **/
@ConfigurationProperties(prefix = "sentinelx")
public class SentinelXProperties {


    private DatasourceProperties datasource;


   static class DatasourceProperties{
       private boolean enable =true;

       public boolean isEnable() {
           return enable;
       }

       public void setEnable(boolean enable) {
           this.enable = enable;
       }
   }
}
