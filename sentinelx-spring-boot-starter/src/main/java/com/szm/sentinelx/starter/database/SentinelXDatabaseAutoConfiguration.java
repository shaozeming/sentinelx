package com.szm.sentinelx.starter.database;


import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author: shaozeming
 * @date: 2022/8/13 12:23
 * @description:  默认规则数据持久化功能自动装配
 **/

@Configuration
@EnableConfigurationProperties(SentinelXDatasourceProperties.class)
@ConditionalOnProperty(name = "sentinelx.datasource.enable",havingValue = "true")
public class SentinelXDatabaseAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public SentinelXDataSourceHandler sentinelXDataSourceHandler(
            DefaultListableBeanFactory beanFactory, SentinelXDatasourceProperties sentinelXDatasourceProperties,
            Environment env) {
        return new SentinelXDataSourceHandler(beanFactory, sentinelXDatasourceProperties, env);
    }



}
