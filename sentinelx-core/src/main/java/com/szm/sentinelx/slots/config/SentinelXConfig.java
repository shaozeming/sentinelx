package com.szm.sentinelx.slots.config;

/**
 * @author: shaozeming
 * @date: 2022/8/18 17:10
 * @description:
 **/
public class SentinelXConfig {

    private static boolean enableCopy = true;


    public static boolean isEnableCopy() {
        return enableCopy;
    }

    public static void setEnableCopy(boolean enableCopy) {
        SentinelXConfig.enableCopy = enableCopy;
    }
}
