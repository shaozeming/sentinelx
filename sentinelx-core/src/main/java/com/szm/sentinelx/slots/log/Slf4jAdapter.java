package com.szm.sentinelx.slots.log;

import com.alibaba.csp.sentinel.log.LogTarget;
import com.alibaba.csp.sentinel.log.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author: shaozeming
 * @date: 2022/9/14 19:13
 * @description:
 **/
@LogTarget
public class Slf4jAdapter implements Logger{

     private static final org.slf4j.Logger LOGGER= LoggerFactory.getLogger(Slf4jAdapter.class);
    /**
     * Log a message at the INFO level according to the specified format
     * and arguments.
     *
     * @param format    the format string
     * @param arguments a list of arguments
     */
    @Override
    public void info(String format, Object... arguments) {
        if(LOGGER.isInfoEnabled()){
            LOGGER.info(format,arguments);
        }
    }

    /**
     * Log an exception (throwable) at the INFO level with an
     * accompanying message.
     *
     * @param msg the message accompanying the exception
     * @param e   the exception (throwable) to log
     */
    @Override
    public void info(String msg, Throwable e) {
        if(LOGGER.isInfoEnabled()) {
            LOGGER.info(msg, e);
        }
    }

    /**
     * Log a message at the WARN level according to the specified format
     * and arguments.
     *
     * @param format    the format string
     * @param arguments a list of arguments
     */
    @Override
    public void warn(String format, Object... arguments) {
        if(LOGGER.isWarnEnabled()) {
            LOGGER.warn(format, arguments);
        }
    }

    /**
     * Log an exception (throwable) at the WARN level with an
     * accompanying message.
     *
     * @param msg the message accompanying the exception
     * @param e   the exception (throwable) to log
     */
    @Override
    public void warn(String msg, Throwable e) {
        if(LOGGER.isWarnEnabled()) {
            LOGGER.warn(msg, e);
        }
    }

    /**
     * Log a message at the TRACE level according to the specified format
     * and arguments.
     *
     * @param format    the format string
     * @param arguments a list of arguments
     */
    @Override
    public void trace(String format, Object... arguments) {
        if(LOGGER.isTraceEnabled()) {
            LOGGER.trace(format, arguments);
        }
    }

    /**
     * Log an exception (throwable) at the TRACE level with an
     * accompanying message.
     *
     * @param msg the message accompanying the exception
     * @param e   the exception (throwable) to log
     */
    @Override
    public void trace(String msg, Throwable e) {
        if(LOGGER.isTraceEnabled()) {
            LOGGER.trace(msg, e);
        }
    }

    /**
     * Log a message at the DEBUG level according to the specified format
     * and arguments.
     *
     * @param format    the format string
     * @param arguments a list of arguments
     */
    @Override
    public void debug(String format, Object... arguments) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(format, arguments);
        }
    }

    /**
     * Log an exception (throwable) at the DEBUG level with an
     * accompanying message.
     *
     * @param msg the message accompanying the exception
     * @param e   the exception (throwable) to log
     */
    @Override
    public void debug(String msg, Throwable e) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(msg, e);
        }
    }

    /**
     * Log a message at the ERROR level according to the specified format
     * and arguments.
     *
     * @param format    the format string
     * @param arguments a list of arguments
     */
    @Override
    public void error(String format, Object... arguments) {
        if(LOGGER.isErrorEnabled()) {
            LOGGER.error(format, arguments);
        }
    }

    /**
     * Log an exception (throwable) at the ERROR level with an
     * accompanying message.
     *
     * @param msg the message accompanying the exception
     * @param e   the exception (throwable) to log
     */
    @Override
    public void error(String msg, Throwable e) {
        if(LOGGER.isErrorEnabled()) {
            LOGGER.error(msg, e);
        }
    }
}
