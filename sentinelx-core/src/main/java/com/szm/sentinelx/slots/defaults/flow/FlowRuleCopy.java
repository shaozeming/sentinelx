package com.szm.sentinelx.slots.defaults.flow;

import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;

/**
 * @author: shaozeming
 * @date: 2022/8/13 10:05
 * @description:
 **/
public class FlowRuleCopy {

    static  FlowRule  replace(String name,FlowRule flowRule){
        FlowRule newRule=new FlowRule();
        newRule.setLimitApp(flowRule.getLimitApp());
        newRule.setStrategy(flowRule.getStrategy());
        newRule.setGrade(flowRule.getGrade());
        newRule.setCount(flowRule.getCount());
        newRule.setWarmUpPeriodSec(flowRule.getWarmUpPeriodSec());
        newRule.setRefResource(flowRule.getRefResource());
        newRule.setControlBehavior(flowRule.getControlBehavior());
        newRule.setClusterConfig(flowRule.getClusterConfig());
        newRule.setClusterMode(flowRule.isClusterMode());
        newRule.setMaxQueueingTimeMs(flowRule.getMaxQueueingTimeMs());
        newRule.setResource(name);
        return newRule;
    }

}
