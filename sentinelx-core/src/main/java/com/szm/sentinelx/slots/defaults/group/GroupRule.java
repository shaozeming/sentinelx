package com.szm.sentinelx.slots.defaults.group;

import com.alibaba.csp.sentinel.slots.block.AbstractRule;

/**
 * @author: shaozeming
 * @date: 2022/8/18 14:18
 * @description:
 **/
public class GroupRule extends AbstractRule {


    private String groupName;


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
