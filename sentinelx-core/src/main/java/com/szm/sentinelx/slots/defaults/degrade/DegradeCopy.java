package com.szm.sentinelx.slots.defaults.degrade;

import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;

/**
 * @author: shaozeming
 * @date: 2022/8/13 10:10
 * @description:
 **/
public class DegradeCopy {

    static DegradeRule replace(String name, DegradeRule degradeRule){
        DegradeRule newRule = new DegradeRule();
        newRule.setTimeWindow(degradeRule.getTimeWindow());
        newRule.setCount(degradeRule.getCount());
        newRule.setGrade(degradeRule.getGrade());
        newRule.setLimitApp(degradeRule.getLimitApp());
        newRule.setMinRequestAmount(degradeRule.getMinRequestAmount());
        newRule.setSlowRatioThreshold(degradeRule.getSlowRatioThreshold());
        newRule.setStatIntervalMs(degradeRule.getStatIntervalMs());
        newRule.setResource(name);
        return newRule;
    }

}
