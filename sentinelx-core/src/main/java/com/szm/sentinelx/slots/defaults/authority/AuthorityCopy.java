package com.szm.sentinelx.slots.defaults.authority;

import com.alibaba.csp.sentinel.slots.block.authority.AuthorityRule;

/**
 * @author: shaozeming
 * @date: 2022/8/13 10:16
 * @description:
 **/
public class AuthorityCopy {

    static AuthorityRule replace(String name, AuthorityRule authorityRule){

        AuthorityRule newRule=new AuthorityRule();
        newRule.setStrategy(authorityRule.getStrategy());
        newRule.setLimitApp(authorityRule.getLimitApp());
        newRule.setResource(name);
        return newRule;
    }
}
